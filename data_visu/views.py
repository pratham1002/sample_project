# Create your views here.
import os
import os.path

import numpy as np
import pyproj
from PIL import Image
# Create your views here.
from django.http import HttpResponse
from django.template import loader
from matplotlib import cm
from numpy import ma
from osgeo import gdal
from osgeo import osr
import matplotlib.colors as colors

from data_visu.constants import input_data_path

def index(request):
    template = loader.get_template('index.html')
    return HttpResponse(template.render())
def view_image_sentinel_2(request):
    image_file = request.GET.get('image_file', '')
    crs = pyproj.Proj(init='EPSG:3857')

    first = int(request.GET.get('band1', ''))
    second = int(request.GET.get('band2', ''))
    third = int(request.GET.get('band3', ''))

    bands = [first, second, third]

    image_file = input_data_path + os.sep + os.path.basename(image_file)
    data_array = gdal.Open(image_file)
    ds = gdal.BuildVRT('', data_array, resolution='highest', srcNodata=0, bandList=bands)

    print(ds)
    if (request.GET.get('bbox', '') != ''):
        BBOX = request.GET.get('bbox', '')
    else:
        BBOX = request.GET.get('BBOX', '')
    request_bbox = [float(t) for t in BBOX.split(',')]

    inSRS_wkt = data_array.GetProjection()  # gives SRS in WKT
    inSRS_converter = osr.SpatialReference()  # makes an empty spatial ref object
    inSRS_converter.ImportFromWkt(inSRS_wkt)  # populates the spatial ref object with our WKT SRS
    proj = inSRS_converter.ExportToProj4()  # Exports an SRS ref as a Proj4 string usable by PyProj

    request_bbox[0], request_bbox[1] = pyproj.transform(crs, proj, request_bbox[0], request_bbox[1])
    request_bbox[2], request_bbox[3] = pyproj.transform(crs, proj, request_bbox[2], request_bbox[3])
    out = gdal.Translate('', ds, format='MEM', strict=True, width=256, height=256,
                         projWin=[request_bbox[0], request_bbox[3], request_bbox[2], request_bbox[1]])
    vis = np.dstack(
        (out.ReadAsArray()[0, ...] / 4000, out.ReadAsArray()[1, ...] / 4000, out.ReadAsArray()[2, ...] / 4000))
    print(vis.shape)
    vis[vis > 1] = 1
    if (np.max(vis) != 0):
        vis11 = [vis > 0]
        vis12 = np.array(vis11)
        bool_val = np.multiply(vis12[0, :, :, 0], 1)
        test = np.dstack((vis, bool_val))
        img = Image.fromarray(np.uint8(test * 255))
        response = HttpResponse(content_type="image/png")
        img.save(response, "PNG")
        return response
    else:
        img = Image.new('RGBA', (256, 256), (0, 0, 0, 0))
        response = HttpResponse(content_type="image/png")
        img.save(response, "PNG")
        return response
