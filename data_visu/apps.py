from django.apps import AppConfig


class DataVisuConfig(AppConfig):
    name = 'data_visu'
