from django.urls import path

from . import views

urlpatterns = [

    # View to create patch
    path('', views.index, name='index'),
    path('view_image_sentinel_2', views.view_image_sentinel_2, name='view_image_sentinel_2'),


]
